import os
import sys
import tskit
import msprime
import numpy as np
import pandas as pd
import seaborn as sns
from copy import deepcopy
import matplotlib.pyplot as plt
from IPython.display import SVG
from os.path import basename, abspath
from tqdm import tqdm
import pickle
import multiprocessing
import time


def run_bash_script(script):
    import subprocess
    return subprocess.check_output(script, shell=True, text=True, executable='/bin/bash')


def sleepy(
    n_simulations: int = 1,
    N: int = 500,
    m: int = 1,
    b: float = 1,
    r: float = 0.0, 
    L: int = 10000, 
    selection_coefficient: float = 0,
    dominance_coefficient: float = 0.5, 
    mutation_position: str = "5000", 
    stop_after_mrca: bool = False, 
    max_generations: int = 10000000,
    condition_on_fixation: bool = False,
    generation_mutation_introduction: int = -1,
    n_generations_post_fixation: int = 0,
    use_tree_sequence_recording: bool = False,
    file_name: str = "file", 
    output_directory: str = "./",
    save_tables: bool = False,
    load_tables: bool = False,
    intermediate_table_generation: int = -1,
    intermediate_filename: str = "intermediate",
    n_parallel: int = 10, 
    continue_from: int = 0, 
    print_cmd: bool = False, # don't run program, but print bash script 
    slurm = False,
    slurm_start = 0, 
    slurm_divide = 2, 
    slurm_limit = 200
) -> None:
    
    """ Python-wrapper for sleepy simulator bash script generation. Also, capable of
    directly running simulations locally if print_cmd is false and slurm is false.
    
    Arg types: 
        * **n_simulations** *(int)* - Number of simulations if script is directly to be run locally.
        * **num_generations** *(int)* - Maximum number of generations (multiplied by gc).
        * **N** *(int)* - Diploid population size N=500 -> 1000 Haplotypes.
        * **m** *(int)* - Number of generations from which seeds can resusicate.
        * **b** *(float)* - Germination rate.
        * **gc** *(int)* - Garbage collection interval.
        * **r** *(int)* - Recombination rate.
        * **L** *(int)* - Mapping length.
        * **s** *(str)* - Selection coefficient.
        * **d** *(str)* - Dominance coefficient.
    """
    
    
    if output_directory[-1] != "/": output_directory += "/"
    
    debug_print=False
    kwargs = { 
        "N":N, 
        "m":m, 
        "b":b, 
        "r":r, 
        "L":L, 
        "generation_mutation_introduction":generation_mutation_introduction,
        "n_generations_post_fixation":n_generations_post_fixation,
        "max_generations":max_generations,
        "selection_coefficient":selection_coefficient,
        "dominance_coefficient":dominance_coefficient, 
        "mutation_position":mutation_position, 
        "stop_after_mrca":stop_after_mrca, 
        "condition_on_fixation": condition_on_fixation,
        "use_tree_sequence_recording":use_tree_sequence_recording, 
        "file_name":file_name, 
        "output_directory":output_directory,
        "save_tables": save_tables,
        "load_tables": load_tables,
        "intermediate_table_generation": intermediate_table_generation,
        "intermediate_filename": intermediate_filename,
        
        
    }
        
    
    cmd_arg_part = ""
    for k,v in zip(kwargs.keys(), kwargs.values()):
        cmd_arg_part += "--" + str(k) + " " + str(v) 
        if k == "file_name":
            if slurm:
                cmd_arg_part += str("_$(($i+$j))")
            else:
                cmd_arg_part += str("_$i")
                
                
        if k == "intermediate_filename":
            if slurm:
                cmd_arg_part += str("_$(($i+$j))")
            else:
                cmd_arg_part += str("_$i")
                
                
        cmd_arg_part += " "
    
    if slurm:
        
        loop = "" 
        increment = 0 + slurm_start
        for i in range(slurm_divide):

            loop += str(int(increment)) + " "
            increment += slurm_limit / slurm_divide


        script = """for j in """ + loop + """; do 
        sleepy  """ + cmd_arg_part + """
        done"""
        
    else:    
        script = """

            n_simulations=""" + str(n_simulations) + """
            n_prog=""" + str(n_parallel) + """
            n_prog_count=0

            i=""" + str(continue_from) + """
            while [[ $i -lt $n_simulations ]]; do

            sleepy """ + cmd_arg_part + """ &
                 (( n_prog_count+=1 ))  
                 [[ $((n_prog_count%n_prog)) -eq 0 ]] && wait
                 (( i+=1 ))
            done
            """
    
    if print_cmd: print(script)
    else: return run_bash_script(script)
    
    
    
#/home/anon/projects2/sleepy/bin/