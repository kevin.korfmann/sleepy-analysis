# sleepy-analysis

Analysis of the weak dormancy model under selection using the sleepy simulator.

See also: https://gitlab.lrz.de/kevin.korfmann/sleepy
